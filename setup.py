from setuptools import setup, find_packages

setup(name='mixedmap',
      version='0.0.1',
      packages=find_packages('src'),
      package_dir={'': 'src'},
      include_package_data=True,

      author=['Perceiving Systems at Max-Planck Institute for Intelligent Systems, Tuebingen'],
      author_email=[],
      maintainer='Nima Ghorbani',
      maintainer_email='nima.gbani@gmail.com',
      url='https://nghorbani.github.io',
      # description='',
      long_description=open("README.md").read(),
      long_description_content_type="text/markdown",
      install_requires=[],
      dependency_links=[
      ],
      classifiers=[
          "Intended Audience :: Research",
          "Natural Language :: English",
          "Operating System :: MacOS :: MacOS X",
          "Operating System :: POSIX",
          "Operating System :: POSIX :: BSD",
          "Operating System :: POSIX :: Linux",
          "Operating System :: Microsoft :: Windows",
          "Programming Language :: Python",
          "Programming Language :: Python :: 3",
          "Programming Language :: Python :: 3.7", ],
      )