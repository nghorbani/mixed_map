# Created by Matthew Loper on 2012-06-05.
# adapted for python 3 by Nima Ghorbani on 2020-06-06
# Copyright (c) 2012 MPI. All rights reserved.


import marshal
import os
import os.path as osp
import pickle
import random
import stat
import string
import subprocess
import sys
import time
from typing import List

from loguru import logger


def makepath(*args, **kwargs):
    '''
    if the path does not exist make it
    :param desired_path: can be path to a file or a folder name
    :return:
    '''
    isfile = kwargs.get('isfile', False)
    desired_path = osp.join(*args)
    if isfile:
        if not osp.exists(osp.dirname(desired_path)): os.makedirs(osp.dirname(desired_path))
    else:
        if not osp.exists(desired_path): os.makedirs(desired_path)
    return desired_path


list_into_chuncks = lambda lst, n: [lst[i:i + n] for i in range(0, len(lst), n)]


# Always env is executed first and then python
# env needs to be executed first to inherit virtualenvironment
# This way python is executed from /home/user/venv/bin/python
# requirements = TARGET.CUDACapability>=7.0


pythonscript = """
import pickle
import os
import types
import marshal
import sys

if __name__ == '__main__':

    func_fname = sys.argv[1]
    input_arg_fname = sys.argv[2]
    index = int(sys.argv[3])
    sys.stderr.write('func_fname=%s\\n'%func_fname)
    sys.stderr.write('input_arg_fname=%s\\n'%input_arg_fname)
    sys.stderr.write('index=%s\\n'%index)
    sys.stderr.write('python=%s\\n'%sys.version)

    with open(input_arg_fname, 'rb') as fp:
        input_args = pickle.load(fp)[index] # this is a list of args
    with open(func_fname, 'rb') as fp:
        func = types.FunctionType(marshal.loads(pickle.load(fp)), globals(), "some_func_name")

    for input_arg in input_args:
        result = func(input_arg)

"""

runscript = """

python_script_fname=$1
func_fname=$2
input_fname=$3
index=$4

deactivate    
conda activate supercap

module load cuda/11.3
module load cudnn/8.2.0-cu11.x

<<PYTHON_PATH>> ${python_script_fname} ${func_fname} ${input_fname} ${index}

exit
"""


def mixedmap(func, seq,
            python_path,
            log_dir, tmp_dir,
            bashrc_fname,
             max_mem=8, jobs_per_instance=1, bid_amount=49,
             use_highend_gpus=False,
             cpu_count=1, gpu_count=0, gpu_memory=None, concurrency_limits=1, username='',
             avoid_nodes: List[str] = None, max_run_time = None, max_auto_retries = None,
             jobs_unique_name=None):
    seq = list_into_chuncks(seq, jobs_per_instance)


    rnd_name = f'{time.strftime("%Y%m%d_%H%M")}_{"".join([random.choice(string.ascii_lowercase) for i in range(4)])}'

    if jobs_unique_name:
        rnd_name = f'{jobs_unique_name}_{rnd_name}'

    tmp_dir = makepath(tmp_dir, rnd_name)

    log_dir = makepath(log_dir, rnd_name)

    while not osp.exists(log_dir):
        time.sleep(2)

    # save inputs to disk
    input_fname = tmp_dir + '/input.pkl'
    with open(input_fname, 'wb') as fp:
        pickle.dump(seq, fp)
    logger.debug(f'Saved cluster inputs to disk: {input_fname}')

    # save our "func" param to disk
    func_fname = tmp_dir + '/callback.pkl'
    with open(func_fname, 'wb') as fp:
        pickle.dump(marshal.dumps(func.__code__), fp)
    logger.info(f'Saved cluster callback function to disk: {func_fname}')

    # get filenames we will use for output
    # output_fnames = [tmp_dir + '/output_%04d.pkl' % (i,) for i in xrange(len(seq))]
    output_fname = tmp_dir + '/output'
    output_fnames = [f'{output_fname}_{iii:d}.pkl' for iii in range(len(seq))]

    # construct caller.py, which will load and call the "func"

    python_fname = tmp_dir + '/pythonscript.py'
    with open(python_fname, 'w') as fp:
        fp.write(pythonscript)
    logger.info(f'Saved caller script to disk: {python_fname}')

    os.chmod(python_fname, stat.S_IXOTH | stat.S_IWOTH | stat.S_IREAD | stat.S_IEXEC)  # make executable

    caller_fname = tmp_dir + '/caller.sh'
    with open(caller_fname, 'w') as fp:
        fp.write(runscript.replace('<<PYTHON_PATH>>', python_path))
    os.chmod(caller_fname, stat.S_IXOTH | stat.S_IWOTH | stat.S_IREAD | stat.S_IEXEC)  # make executable

    requirements = []
    if gpu_count < 0:
        requirements.append("TotalGPUs =?= 0")
    else:
        if use_highend_gpus:
            requirements.append("TARGET.CUDACapability>=7.0")
        if gpu_count > 0 and gpu_memory:
            requirements.append("TARGET.CUDAGlobalMemoryMb > {}".format(gpu_memory * 1024))
    if avoid_nodes:
        for node in avoid_nodes:
            requirements.append(f'UtsnameNodename = != "{node}"')

    requirements = "&&".join(requirements)

    cs = """"""
    # cs = cs.replace('<<EXE>>', 'env')
    # cs = cs.replace('<<EXE>>', '/home/gpons/venv_geist/bin/python')
    cs += f'executable = {caller_fname}\n'
    cs += f'arguments = {python_fname} {func_fname} {input_fname} $(Process)\n'
    cs += f'error = {log_dir}/error.$(Process).err\n'
    cs += f'output = {log_dir}/output.$(Process).out\n'
    cs += f'request_memory = {int(max_mem * 1024)}\n'
    cs += f'request_cpus = {int(cpu_count)}\n'
    if gpu_count > 0:
        cs += f'request_gpus = {int(gpu_count)}\n'
    if concurrency_limits > 0:
        cs += f'concurrency_limits=user.mytag:{concurrency_limits}\n'
    if len(requirements):
        cs += f'requirements = {requirements}\n'
    if max_run_time and max_run_time > 0:
        if max_auto_retries and max_auto_retries >0:
            cs += f'periodic_hold = (JobStatus =?= 2) && ((CurrentTime - JobCurrentStartDate) >= {int(max_run_time*3600)})\n'
            cs += f'periodic_hold_reason = ifThenElse(JobRunCount <= {max_auto_retries}, "Job runtime exceeded", "Job runtime exceeded, no more retries left")\n'
            cs += f'periodic_hold_subcode = ifThenElse(JobRunCount <= {max_auto_retries}, 1, 2)\n'
            cs += 'periodic_remove = ( (JobStatus =?= 5) && (HoldReasonCode =?= 3) && (HoldReasonSubCode =?= 2) )\n'
            cs += f'periodic_release = ( (JobStatus =?= 5) && (HoldReasonCode =?= 3) && (HoldReasonSubCode =?= 1) )\n'

        else:
            cs += f'periodic_remove = (JobStatus =?= 2) && ((CurrentTime - JobCurrentStartDate) >= {int(max_run_time*3600)})\n'

    cs += f'getenv = True\n'
    cs += f'queue {len(seq)}\n'

    condor_fname = tmp_dir + '/condor_script.sub'
    with open(condor_fname, 'w') as fp:
        fp.write(cs)
    os.chmod(condor_fname, stat.S_IXOTH | stat.S_IWOTH | stat.S_IREAD | stat.S_IEXEC)  # make executable

    # submit jobs
    logger.info('Submitting jobs')
    # cmd = 'qsub -e %s -p %d -o %s -b y -pe parallel 1 -R y -l h_vmem=%.2fG -t %d-%d -j y %s' % \
    #       (stderr_dir, priority, stdout_dir, max_mem, 1, 1+len(seq), caller_fname,)

    cmd = f'source {bashrc_fname}; conda activate supercap; condor_submit_bid {bid_amount:d} {condor_fname}'
    # cmd = 'source ~/.profile; condor_submit_bid 200 %s' % (condor_fname,)

    subprocess.call(["ssh", f"{username}@login.cluster.is.localnet"] + [cmd])

    return
